import firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyBkBvR6HDmX9UZ0_7GNxqR-uhYk1KECU8Q",
  authDomain: "rfid-project3.firebaseapp.com",
  databaseURL: "https://rfid-project3.firebaseio.com",
  projectId: "rfid-project3",
  storageBucket: "rfid-project3.appspot.com",
  messagingSenderId: "852159900892",
  appId: "1:852159900892:web:3d89521cbdcfc1720b9819",
  measurementId: "G-4D371069C6"
};

firebase.initializeApp(firebaseConfig);

const db = firebase.database()

const storeVolt = (volt) => db.ref(`volt/${volt}`).set(volt)

export {db};

export default {
	storeVolt
};